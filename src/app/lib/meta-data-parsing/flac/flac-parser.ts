import { SongMeta } from 'src/app/model/song-meta.model';
import { BinaryFileReader } from '../binary-file-reader';
import { MetaTagParser } from '../meta-tag-parser';

const BLOCK_TYPE_VORBIS_COMMENT = 4;
const BLOCK_TYPE_VORBIS_PICTURE = 6;

export class FlacParser implements MetaTagParser {
  constructor(private fileReader: BinaryFileReader) {
  }

  // file starts with fLaC
  canParse(): boolean {
    return this.fileReader.readByte(0) === 0x66
      && this.fileReader.readByte(1) === 0x4C
      && this.fileReader.readByte(2) === 0x61
      && this.fileReader.readByte(3) === 0x43;
  }

  parse(): SongMeta {
    const header = this.readHeader(4, {});

    if (header !== undefined) {
      const tags = this.readTags(header);

      return {
        title: tags['TITLE'],
        artist: tags['ARTIST'],
        album: tags['ALBUM'],
        track: tags['TRACKNUMBER'],
        year: tags['DATE']
      };
    } else {
      return {};
    }
  }

  private readHeader(offset, collectingHeader): any {
    if (offset < this.fileReader.byteLength) {
      const blockHeader = this.fileReader.readByte(offset);

      const blockSize = this.fileReader.readNumber(offset + 1, 3);

      if (blockHeader === BLOCK_TYPE_VORBIS_COMMENT) {
        collectingHeader.commentOffset = offset + 4;
      } else if (blockHeader === BLOCK_TYPE_VORBIS_PICTURE) {

      } else {
        // ignore
      }

      if (blockHeader <= 127) {
        return this.readHeader(offset + 4 + blockSize, collectingHeader);
      } else {
        return collectingHeader;
      }
    } else {
      return undefined;
    }
  }

  private readTags(header: any): any {
    const result = {};

    const commentOffset = header['commentOffset'];
    const vendorLength = this.fileReader.readNumber(commentOffset, 4, 8, false);

    let offset = commentOffset + 4 + vendorLength;
    const numberOfComments = this.fileReader.readNumber(offset, 4, 8, false);

    offset += 4;
    for (let i = 0; i < numberOfComments; i++) {
      const dataLength = this.fileReader.readNumber(offset, 4, 8, false);
      offset += 4;
      const fullString = this.fileReader.readStringUtf8(offset, dataLength);

      const tagName = fullString.slice(0, fullString.indexOf('='));
      const tagValue = fullString.slice(fullString.indexOf('=') + 1);

      result[tagName] = tagValue;

      offset += dataLength;
    }

    return result;
  }
}
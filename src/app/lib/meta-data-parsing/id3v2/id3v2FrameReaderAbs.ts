import { BinaryFileReader } from '../binary-file-reader';

export abstract class Id3v2FrameReaderAbs {
  constructor(protected fileReader: BinaryFileReader) {
  }

  abstract readFrames(offset: number, length: number, header: any): any;

  protected readFrameFlags(offset: number): any {
    const frameFlags = {};

    frameFlags['tagAlterPreservation'] = this.fileReader.isBitSet(offset, 7);
    frameFlags['fileAlterPreservation'] = this.fileReader.isBitSet(offset, 6);
    frameFlags['readOnly'] = this.fileReader.isBitSet(offset, 5);

    offset += 1;
    frameFlags['compression'] = this.fileReader.isBitSet(offset, 7);
    frameFlags['encryption'] = this.fileReader.isBitSet(offset, 6);
    frameFlags['groupingIdentity'] = this.fileReader.isBitSet(offset, 5);

    return frameFlags;
  }

  protected readFrameDataAsText(offset: number, dataLength: number): string {
    const charset = this.readTextCharset(offset);
    offset += 1;
    dataLength -= 1;

    if (charset === 'utf-16') {
      return this.fileReader.readStringUtf16(offset, dataLength);
    } else {
      return this.fileReader.readString(offset, dataLength);
    }
  }

  protected readTextCharset(offset: number): string {
    if (this.fileReader.readByte(offset) === 0) {
      return 'iso-8859-1';
    } else if (this.fileReader.readByte(offset) === 1) {
      return 'utf-16';
    } else {
      return '';
    }
  }
}
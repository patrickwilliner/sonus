import { Id3v2FrameReaderAbs } from './id3v2FrameReaderAbs';

export class Id3v23FrameReader extends Id3v2FrameReaderAbs {
  readFrames(offset: number, length: number, header: any): any {
    const result = {};

    if (header['flags']['extendedHeader']) {
      offset += header['extendedHeader']['size'];
    }

    while (offset < length) {
      let frameIdLength: number;
      let frameSize: number;
      let frameHeaderSize: number;

      frameIdLength = 4;
      frameSize = this.fileReader.readNumber(offset + 4, 4);
      frameHeaderSize = 10;

      const frameId = this.fileReader.readString(offset, frameIdLength);

      if (frameId !== '') {
        const frameFlags = this.readFrameFlags(offset + 4 + 4);

        offset += frameHeaderSize;

        let frameData: any;
        if (frameId.toLocaleLowerCase().indexOf('t') === 0) {
          frameData = this.readFrameDataAsText(offset, frameSize);
        }

        if (frameId.toLowerCase() === 'apic') {
          frameData = this.readFrameDataAsPicture(offset, frameSize, header);
        }

        result[frameId] = {
          size: frameSize,
          frameFlags: frameFlags,
          data: frameData
        };

        offset += frameSize;
      } else {
        break;
      }
    }

    return result;
  }

  private readFrameDataAsPicture(offset: number, dataLength: number, header: any): any {
    const origOffset = offset;
    const charset = this.readTextCharset(offset);

    // MIME type: <text string> $00
    offset += 1;
    const mimeType = this.fileReader.readString(offset, dataLength);

    offset += mimeType.length + 1;

    const pictureType = this.fileReader.readByte(offset);

    offset += 1;
    let description: string;

    if (charset === 'utf-16') {
      description = this.fileReader.readStringUtf16(offset, 64);
    } else {
      description = this.fileReader.readString(offset, 64);
    }

    offset = offset + 1 + description.length;
    if (charset === 'utf-16') {
      offset !== description.length + 1;
    }

    const pictureData = this.fileReader.readBytes(offset, dataLength);

    return {
      pictureType: pictureType,
      mimeType: mimeType,
      charset: charset,
      description: description,
      data: pictureData
    };
  }
}
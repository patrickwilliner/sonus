import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { SongMeta } from 'src/app/model/song-meta.model';
import { BinaryFileReader } from './binary-file-reader';
import { Id3v2Parser } from './id3v2/id3v2-parser';
import { FlacParser } from './flac/flac-parser';

export default class MetaDataParser {
  parse(file: File): Observable<SongMeta> {
    return BinaryFileReader.create(file, 1024)
      .pipe(first())
      .pipe(map(fileReader => {
        const id3v2Parser = new Id3v2Parser(fileReader);
        if (id3v2Parser.canParse()) {
          return id3v2Parser.parse();
        } else {
          const flacParser = new FlacParser(fileReader);
          if (flacParser.canParse()) {
            return flacParser.parse();
          }
        }

        return {};
      }));
  }
}
import { SongMeta } from 'src/app/model/song-meta.model';
import { BinaryFileReader } from '../binary-file-reader';
import { MetaTagParser } from '../meta-tag-parser';

const META_TAG = 'ID3v1';

export default class Id3v1Parser implements MetaTagParser {
  constructor(private fileReader: BinaryFileReader) {
  }

  canParse(): boolean {
    const offset = this.fileReader.byteLength - 128;
    return this.fileReader.readString(offset, 3) === 'TAG';
  }

  parse(): SongMeta {
    const songMeta: SongMeta = {
    };

    let offset = this.fileReader.byteLength - 128 + 3;
    songMeta.title = this.fileReader.readString(offset, 30);

    offset += 30;
    songMeta.artist = this.fileReader.readString(offset, 30);

    offset += 30;
    songMeta.album = this.fileReader.readString(offset, 30);

    offset += 30;
    songMeta.year = this.fileReader.readString(offset, 4);

    songMeta.metaTag = META_TAG;

    return songMeta;
  }
}
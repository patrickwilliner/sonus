import { SongMeta } from 'src/app/model/song-meta.model';

export interface MetaTagParser {
  canParse(): boolean;
  parse(): SongMeta;
}
// type defs for Media Session API 1.0
// defs: https://github.com/DefinitelyTyped/DefinitelyTyped

// interface Navigator {
//   readonly mediaSession?: MediaSession;
// }

interface Window {
  MediaSession?: MediaSession;
}

// type MediaSessionPlaybackState = 'none' | 'paused' | 'playing';

// type MediaSessionAction = 'play' | 'pause' | 'seekbackward' | 'seekforward' | 'seekto' | 'previoustrack' | 'nexttrack' | 'skipad' | 'stop';

interface MediaSession {
  playbackState: MediaSessionPlaybackState;
  metadata: MediaMetadata | null;
  setActionHandler(action: MediaSessionAction, listener: (() => void) | null): void;
}

interface MediaImage {
  src: string;
  sizes?: string;
  type?: string;
}

interface MediaMetadataInit {
  title?: string;
  artist?: string;
  album?: string;
  artwork?: MediaImage[];
}

// declare class MediaMetadata {
//   constructor(init?: MediaMetadataInit);
//   title: string;
//   artist: string;
//   album: string;
//   artwork: MediaImage[];
// }
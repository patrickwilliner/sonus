export class ImageUtil {
  public static buildBlobUrl(bytes: number[], mimeType: string): string {
    const arrayBufferView = new Uint8Array(bytes);
    const blob = new Blob([arrayBufferView], { type: mimeType });
    const urlCreator = window.URL || window.webkitURL;
    return urlCreator.createObjectURL(blob);
  }
}
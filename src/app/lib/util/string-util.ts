const DEFAULT_COMPARE_OPTIONS = {
  ignoreCase: false,
  trim: false
};

export class StringUtil {
  static compare(s1: string, s2: string, options = DEFAULT_COMPARE_OPTIONS): number {
    if (options.ignoreCase) {
      s1 = s1.toUpperCase();
      s2 = s2.toUpperCase();
    }
    if (options.trim) {
      s1 = s1.trim();
      s2 = s2.trim();
    }

    if (s1 === s2) {
      return 0;
    } else if (s1 < s2) {
      return -1;
    } else {
      return 1;
    }
  }

  static getComparator(options: { ignoreCase: boolean, trim: boolean }): (s1: string, s2: string) => number {
    return (s1: string, s2: string) => {
      return this.compare(s1, s2, options);
    }
  }
}
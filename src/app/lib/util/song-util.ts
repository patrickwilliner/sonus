import { Song } from 'src/app/model/song.model';
import { SongMeta } from 'src/app/model/song-meta.model';
import { StringUtil } from 'src/app/lib/util/string-util';

export class SongUtil {
  private static stringComparator = StringUtil.getComparator({ ignoreCase: true, trim: true });

  static compare(song1: Song, song2: Song): number {
    if (song1 === song2) {
      return 0;
    } else {
      return SongUtil.stringComparator(song1.fileMeta.name, song2.fileMeta.name);
    }
  }

  static compareSongMeta(meta1: SongMeta, meta2: SongMeta): number {
    if (meta1 === meta2) {
      return 0;
    } else {
      if (SongUtil.canCompareMetaTrack(meta1, meta2)) {
        return SongUtil.stringComparator(meta1.track, meta2.track);
      } else {
        return SongUtil.stringComparator(meta1.title, meta2.title);
      }
    }
  }

  private static canCompareMetaTrack(meta1: SongMeta, meta2: SongMeta): boolean {
    return SongUtil.stringComparator(meta1.artist, meta2.artist) === 0
      && SongUtil.stringComparator(meta1.album, meta2.album) === 0;
  }
}
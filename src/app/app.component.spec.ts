import { async, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { AppComponent } from './app.component';
import { NotificationComponent } from './components/notification/notification.component';
import { StatusComponent } from './components/status/status.component';
import { PlayerStatus } from './model/player-status.model';
import { BusinessDelegateService } from './services/business-delegate.service';
import { ConfigService } from './services/config.service';
import { InitService } from './services/init.service';
import { LibraryService } from './services/library.service';
import { NotificationService } from './services/notification.service';
import { PlayerService } from './services/player.service';
import { PlaylistService } from './services/playlist.service';
import { ProgressService } from './services/progress.service';
import { RadioService } from './services/radio.service';
import { TimerService } from './services/timer.service';
import { RepeatButtonComponent } from './components/repeat-button/repeat-button.component';
import { PluralizePipe } from './pipes/pluralize.pipe';
import { PlayerComponent } from './components/player/player.component';
import { ShuffleButtonComponent } from './components/shuffle-button/shuffle-button.component';
import { SpectrumAnimatorComponent } from './components/spectrum-animator/spectrum-animator.component';
import { AudioService } from './services/audio.service';
import { DurationPipe } from './pipes/duration.pipe';
import { SliderComponent } from './components/slider/slider.component';
import { TimerComponent } from './components/timer/timer.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { PlayingNowComponent } from './components/playing-now/playing-now.component';
import { ActionPanelComponent } from './components/action-panel/action-panel.component';
import { ConfigButtonComponent } from './components/config-button/config-button.component';
import { LoadButtonComponent } from './components/load-button/load-button.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NotificationComponent,
        StatusComponent,
        RepeatButtonComponent,
        PluralizePipe,
        PlayerComponent,
        ShuffleButtonComponent,
        SpectrumAnimatorComponent,
        DurationPipe,
        SliderComponent,
        TimerComponent,
        CheckboxComponent,
        PlayingNowComponent,
        ActionPanelComponent,
        ConfigButtonComponent,
        LoadButtonComponent
      ],
      providers: [
        { provide: TimerService, useValue: new TimerService(undefined, createPlayerServiceMock()) },
        { provide: InitService, useValue: {} },
        { provide: PlayerService, useValue: new PlayerService(undefined, undefined, undefined) },
        { provide: ConfigService, useValue: new ConfigService() },
        { provide: PlaylistService, useValue: new PlaylistService() },
        { provide: RadioService, useValue: new RadioService() },
        { provide: LibraryService, useValue: new LibraryService(undefined, undefined) },
        { provide: BusinessDelegateService, useValue: {} },
        { provide: NotificationService, useValue: new NotificationService() },
        { provide: ProgressService, useValue: new ProgressService() },
        { provide: AudioService, useValue: new AudioService() }]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});

function createPlayerServiceMock(): PlayerService {
  let playerServiceMock = jasmine.createSpyObj(PlayerService, ['getPlayerStatus$']);
  playerServiceMock.getPlayerStatus$.and.returnValue(new Observable<PlayerStatus>());
  return playerServiceMock
}
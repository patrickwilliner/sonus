import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Config } from './model/config.model';
import { Library } from './model/library.model';
import { PlayerStatus } from './model/player-status.model';
import { Playlist } from './model/playlist.model';
import { RadioStation } from './model/radio-station.model';
import { Radio } from './model/radio.model';
import { Song } from './model/song.model';
import { BusinessDelegateService } from './services/business-delegate.service';
import { ConfigService } from './services/config.service';
import { InitService } from './services/init.service';
import { LibraryService } from './services/library.service';
import { PlayerService } from './services/player.service';
import { PlaylistService } from './services/playlist.service';
import { ProgressService } from './services/progress.service';
import { RadioService } from './services/radio.service';
import { TimerService } from './services/timer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  playlist$: Observable<Playlist>;
  playerStatus$: Observable<PlayerStatus>;
  config$: Observable<Config>;
  radio$: Observable<Radio>;
  library$: Observable<Library>;
  progress$: Observable<boolean>;
  timer$: Observable<number>;

  libraryEnabled: boolean;
  radioEnabled: boolean;

  constructor(timerService: TimerService,
              private initService: InitService,
              private playerService: PlayerService,
              private configService: ConfigService,
              private playlistService: PlaylistService,
              private radioService: RadioService,
              private libraryService: LibraryService,
              private businessDelegateService: BusinessDelegateService,
              progressService: ProgressService) {
    this.playlist$ = playlistService.getPlaylist$();
    this.playerStatus$ = playerService.getPlayerStatus$();
    this.config$ = configService.getConfig$();
    this.radio$ = radioService.getRadio$();
    this.library$ = libraryService.getLibrary$();
    this.timer$ = timerService.getTimer();
    this.progress$ = progressService.isEnabled$();
  }

  ngOnInit(): void {
    this.initService.init();
    this.libraryEnabled = this.libraryService.isEnabled();
    this.radioEnabled = this.radioService.isEnabled();
  }

  onLoadFiles(fileList: FileList): void {
    this.businessDelegateService.loadFilesToSessionPlaylist(fileList);
  }

  onClickPlay(): void {
    this.playerService.play();
  }

  onClickPause(): void {
    this.playerService.pause();
  }

  onClickPrevious(): void {
    this.playerService.playPrevious();
  }

  onClickNext(): void {
    this.playerService.playNext();
  }

  onClickSong(index: number): void {
    this.playerService.playSong(index);
  }

  onSliderNavigate(time: number): void {
    this.playerService.navigateToTime(time);
  }

  onChangeConfig(config: Config): void {
    this.configService.updateConfig(config);
  }

  onChangeTimerMode(mode: boolean): void {
    this.configService.updateConfig({ timerModeIsElapsedTime: mode });
  }

  onToggleShuffle(): void {
    this.playlistService.toggleShuffle();
  }

  onToggleRepeat(): void {
    this.playlistService.toggleRepeatMode();
  }

  onClickRadioStation(radioStation: RadioStation): void {
    this.radioService.playStream(radioStation);
  }

  onStoreActivePlaylist(): void {
    this.libraryService.saveSessionPlaylist();
  }

  onUpdateStoredPlaylist(playlist: Playlist): void {
    this.libraryService.updatePlaylist(playlist);
  }

  onPlayLibraryPlaylist(playlist: Playlist): void {
    this.businessDelegateService.playLibraryPlaylist(playlist);
  }

  onDeleteLibraryPlaylist(playlists: Playlist[]): void {
    this.libraryService.deletePlaylists(playlists);
  }

  getCurrentSong(playlist: Playlist): Song {
    return playlist?.songs[playlist.selectedSong];
  }
}
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-load-button',
  templateUrl: './load-button.component.html',
  styleUrls: ['./load-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadButtonComponent implements OnInit {
  @Output()
  loadFiles = new EventEmitter<FileList>();

  @ViewChild('fileInput')
  fileInputElement: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
  }

  onClick(): void {
    this.fileInputElement.nativeElement.click();
  }

  onFileInputChange(event): void {
    if (event?.target?.files?.length > 0) {
      this.loadFiles.emit(event.target.files);
    }
  }
}
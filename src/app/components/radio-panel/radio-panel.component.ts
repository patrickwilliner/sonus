import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RadioStation } from 'src/app/model/radio-station.model';

@Component({
  selector: 'app-radio-panel',
  templateUrl: './radio-panel.component.html',
  styleUrls: ['./radio-panel.component.scss']
})
export class RadioPanelComponent implements OnInit {
  @Input()
  radioStations: RadioStation[];

  @Output()
  clickRadioStation = new EventEmitter<RadioStation>();

  constructor() { }

  ngOnInit(): void {
  }

  onClickRadioStation(radioStation: RadioStation): void {
    this.clickRadioStation.emit(radioStation);
  }
}
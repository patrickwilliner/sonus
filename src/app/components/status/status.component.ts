import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Playlist } from 'src/app/model/playlist.model';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusComponent {
  @Input()
  playlist: Playlist;

  getPlaylistSize(): number {
    return this.playlist?.songs?.length || 0;
  }

  getTotalTime(): number {
    let totalTime = 0;
    this.playlist?.songs?.forEach(song => {
      totalTime += song.songMeta.duration;
    });
    return totalTime;
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DurationPipe } from 'src/app/pipes/duration.pipe';
import { PluralizePipe } from 'src/app/pipes/pluralize.pipe';
import { StatusComponent } from './status.component';


describe('StatusComponent', () => {
  let component: StatusComponent;
  let fixture: ComponentFixture<StatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusComponent, PluralizePipe, DurationPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

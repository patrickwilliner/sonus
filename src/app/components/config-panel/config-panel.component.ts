import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Config } from 'src/app/model/config.model';

@Component({
  selector: 'app-config-panel',
  templateUrl: './config-panel.component.html',
  styleUrls: ['./config-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigPanelComponent {
  @Input()
  config: Config;

  @Output()
  changeConfig = new EventEmitter<Config>();

  onChangeAutoplayOnFileLoad(value: boolean): void {
    this.changeConfig.emit({
      autoplayOnFileLoad: value
    });
  }

  onChangeRestorePlaylistOnStartup(value: boolean): void {
    this.changeConfig.emit({
      restorePlaylistOnStartup: value
    });
  }

  onAudioVolumeChange(value: number): void {
    this.changeConfig.emit({
      audioVolume: value
    });
  }
}
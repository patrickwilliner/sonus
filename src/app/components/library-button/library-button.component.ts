import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-library-button',
  templateUrl: './library-button.component.html',
  styleUrls: ['./library-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LibraryButtonComponent {
  @Input()
  buttonEnabled = false;

  @Output()
  clickLibrary = new EventEmitter<void>();

  onClick(): void {
    this.clickLibrary.emit();
  }
}
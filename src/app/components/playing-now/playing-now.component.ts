import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Playlist } from 'src/app/model/playlist.model';

@Component({
  selector: 'app-playing-now',
  templateUrl: './playing-now.component.html',
  styleUrls: ['./playing-now.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayingNowComponent {
  @Input()
  playlist$: Observable<Playlist>;

  getArtist(playlist: Playlist): string {
    return playlist?.songs[playlist.selectedSong]?.songMeta.artist;
  }

  getSongName(playlist: Playlist): string {
    return playlist?.songs[playlist.selectedSong]?.songMeta.title
      || playlist?.songs[playlist.selectedSong]?.fileMeta.name;
  }
}
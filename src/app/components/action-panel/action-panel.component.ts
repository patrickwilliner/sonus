import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { Library } from 'src/app/model/library.model';
import { Playlist } from 'src/app/model/playlist.model';
import { RadioStation } from 'src/app/model/radio-station.model';
import { Radio } from 'src/app/model/radio.model';
import { Config } from '../../model/config.model';


@Component({
  selector: 'app-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.scss']
})
export class ActionPanelComponent {
  @Input()
  config: Config;

  @Input()
  radio: Radio;

  @Input()
  library: Library;

  @Input()
  libraryEnabled = false;

  @Input()
  radioEnabled = false;

  @Output()
  changeConfig = new EventEmitter<Config>();

  @Output()
  clickRadioStation = new EventEmitter<RadioStation>();

  @Output()
  storeActivePlaylist = new EventEmitter<void>();

  @Output()
  updateStoredPlaylist = new EventEmitter<Playlist>();

  @Output()
  play = new EventEmitter<Playlist>();

  @Output()
  clickLoadFiles = new EventEmitter<FileList>();

  @Output()
  deleteLibraryPlaylist = new EventEmitter<Playlist[]>();

  configPanelOpen = false;
  radioPanelOpen = false;
  libraryPanelOpen = false;

  constructor(private hostElementRef: ElementRef) {
  }

  @HostListener('document:click', ['$event'])
  onClickDocument(event: any): void {
    if (/*this.panelOpen && */this.isOutsideClick(event)) {
    }
  }

  onClickLibrary(): void {
    this.libraryPanelOpen = !this.libraryPanelOpen;
    this.configPanelOpen = false;
    this.radioPanelOpen = false;
  }

  onClickRadio(): void {
    this.radioPanelOpen = !this.radioPanelOpen;
    this.configPanelOpen = false;
    this.libraryPanelOpen = false;
  }

  onClickConfig(): void {
    this.configPanelOpen = !this.configPanelOpen;
    this.libraryPanelOpen = false;
    this.radioPanelOpen = false;
  }

  onPlayPlaylist(playlist: Playlist): void {
    this.libraryPanelOpen = false;
    this.play.emit(playlist);
  }

  private isOutsideClick(event: any): boolean {
    return !this.hostElementRef.nativeElement.contains(event.target);
  }
}
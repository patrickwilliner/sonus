import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActionTabComponent } from '../action-tab/action-tab.component';
import { ConfigButtonComponent } from '../config-button/config-button.component';
import { ConfigPanelComponent } from '../config-panel/config-panel.component';
import { LibraryButtonComponent } from '../library-button/library-button.component';
import { LibraryPanelComponent } from '../library-panel/library-panel.component';
import { LoadButtonComponent } from '../load-button/load-button.component';
import { RadioButtonComponent } from '../radio-button/radio-button.component';
import { RadioPanelComponent } from '../radio-panel/radio-panel.component';
import { ActionPanelComponent } from './action-panel.component';


describe('ActionPanelComponent', () => {
  let component: ActionPanelComponent;
  let fixture: ComponentFixture<ActionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActionPanelComponent, ActionTabComponent, ConfigPanelComponent, RadioPanelComponent, LibraryPanelComponent, LoadButtonComponent, LibraryButtonComponent, RadioButtonComponent, ConfigButtonComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

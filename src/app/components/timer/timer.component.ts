import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Song } from 'src/app/model/song.model';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent {
  @Input()
  timer$: Observable<number>;

  @Input()
  song: Song;

  @Input()
  modeIsElapsedTime = true;

  @Output()
  changeMode = new EventEmitter<boolean>();

  onClickTime(): void {
    this.changeMode.emit(!this.modeIsElapsedTime);
  }

  getTime(time: number): number {
    if (!this.modeIsElapsedTime) {
      return this.song?.songMeta?.duration - time;
    } else {
      return time;
    }
  }
}
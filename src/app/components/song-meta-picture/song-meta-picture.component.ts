import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { SongMeta } from 'src/app/model/song-meta.model';

@Component({
  selector: 'app-song-meta-picture',
  templateUrl: './song-meta-picture.component.html',
  styleUrls: ['./song-meta-picture.component.scss']
})
export class SongMetaPictureComponent implements OnInit, OnChanges {
  @Input()
  songMeta: SongMeta;

  @ViewChild('picture')
  pictureElement: ElementRef;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.setPicture();
  }

  setPicture(): void {
    if (this.songMeta != null && this.songMeta.picture != null) {
      const arrayBufferView = new Uint8Array(this.songMeta.picture['data']);
      const blob = new Blob([arrayBufferView], { type: this.songMeta.picture['type'] });
      const urlCreator = window.URL || window.webkitURL;
      const imageUrl = urlCreator.createObjectURL(blob);
      this.pictureElement.nativeElement.src = imageUrl;
    }
  }
}
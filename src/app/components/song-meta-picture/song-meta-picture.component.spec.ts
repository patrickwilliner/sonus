import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SongMetaPictureComponent } from './song-meta-picture.component';

describe('SongMetaPictureComponent', () => {
  let component: SongMetaPictureComponent;
  let fixture: ComponentFixture<SongMetaPictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SongMetaPictureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SongMetaPictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

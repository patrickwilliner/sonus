import { Component } from '@angular/core';
import { NotificationService } from 'src/app/services/notification.service';
import { Notification } from '../../model/notification.model';
import { NotificationAnimation } from './notification.animation';

const AUTO_DISMISS_DELAY_MS = 3000;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [NotificationAnimation]
})
export class NotificationComponent {
  notifications: Notification[] = [];

  constructor(private notificationService: NotificationService) {
    notificationService.getNotifications$().subscribe(notification => {
      this.notifications.push(notification);
      this.dismissNotificationAfterDelay(notification, AUTO_DISMISS_DELAY_MS);
    });
  }

  dismiss(notification: Notification): void {
    this.notifications.splice(this.notifications.indexOf(notification), 1);
  }

  private dismissNotificationAfterDelay(notification: Notification, delayMillis: number): void {
    setTimeout(() => {
      this.dismiss(notification);
    }, delayMillis);
  }
}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { empty } from 'rxjs';
import { NotificationService } from 'src/app/services/notification.service';
import { NotificationComponent } from './notification.component';


const notificationServiceMock = {
  getNotifications$: () => {
    return empty();
  }
}

describe('NotificationComponent', () => {
  let component: NotificationComponent;
  let fixture: ComponentFixture<NotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationComponent],
      providers: [{ provide: NotificationService, useValue: notificationServiceMock }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

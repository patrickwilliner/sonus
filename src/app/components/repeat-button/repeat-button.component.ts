import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { RepeatMode } from 'src/app/model/repeat-status.model';

@Component({
  selector: 'app-repeat-button',
  templateUrl: './repeat-button.component.html',
  styleUrls: ['./repeat-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RepeatButtonComponent {
  @Input()
  repeatMode: RepeatMode;

  @Output()
  toggle = new EventEmitter<void>();

  onClick(): void {
    this.toggle.emit();
  }

  repeatPlaylistEnabled(): boolean {
    return this.repeatMode === RepeatMode.PLAYLIST;
  }

  shouldShowRepeatPlaylistIcon(): boolean {
    return this.repeatMode == null || this.repeatMode === RepeatMode.NONE || this.repeatMode === RepeatMode.PLAYLIST;
  }

  shouldShowRepeatSongIcon(): boolean {
    return this.repeatMode === RepeatMode.CURRENT_SONG;
  }
}
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-shuffle-button',
  templateUrl: './shuffle-button.component.html',
  styleUrls: ['./shuffle-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShuffleButtonComponent {
  @Input()
  isEnabled: boolean;

  @Output()
  toggle = new EventEmitter<void>();

  onClick(): void {
    this.toggle.emit();
  }
}
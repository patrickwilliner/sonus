import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Playlist } from 'src/app/model/playlist.model';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistComponent implements OnInit {
  @Input()
  playlist$: Observable<Playlist>;

  @Output()
  clickSong = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
  }

  isSelected(index: number): boolean {
    return false;
  }
}
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Song } from 'src/app/model/song.model';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements AfterViewInit, OnDestroy {
  @Input()
  timer$: Observable<number>;

  @Input()
  song: Song;

  @Output()
  navigate = new EventEmitter<number>();

  @ViewChild('progressbar')
  svgProgressBar: ElementRef;

  @ViewChild('svg')
  svgElement: ElementRef;

  private timerSubscription: Subscription;

  constructor() {
  }

  ngAfterViewInit(): void {
    this.timerSubscription = this.timer$?.subscribe(time => {
      if (this.song != null) {
        this.svgProgressBar.nativeElement.setAttribute('x2', this.getElapsedTimePercent(this.song?.songMeta.duration, time));
      }
    });
  }

  ngOnDestroy(): void {
    this.timerSubscription?.unsubscribe();
  }

  onMouseClick(mouseEvent: MouseEvent): void {
    const width = this.svgElement.nativeElement.getBoundingClientRect().width;
    const componentOffsetX = mouseEvent['layerX'];
    this.navigate.emit(componentOffsetX / width * this.song.songMeta.duration);
  }

  private getElapsedTimePercent(duration: number, currentTime: number) {
    return currentTime > 0 ? currentTime * 100 / duration : 0;
  }
}
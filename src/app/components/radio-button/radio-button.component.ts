import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss']
})
export class RadioButtonComponent {
  @Input()
  buttonEnabled: boolean;

  @Output()
  clickRadio = new EventEmitter<void>();

  onClick(): void {
    this.clickRadio.emit();
  }
}
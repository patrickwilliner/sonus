import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-config-button',
  templateUrl: './config-button.component.html',
  styleUrls: ['./config-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigButtonComponent {
  @Input()
  buttonEnabled = false;

  @Output()
  clickConfig = new EventEmitter<void>();

  onClick(): void {
    this.clickConfig.emit();
  }
}
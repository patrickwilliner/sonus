import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-volume',
  templateUrl: './volume.component.html',
  styleUrls: ['./volume.component.scss']
})
export class VolumeComponent implements AfterViewInit, OnChanges {
  @Input()
  value = 1;

  @Output()
  valueChange = new EventEmitter<number>();

  @ViewChild('progressbar')
  svgProgressBar: ElementRef;

  @ViewChild('svg')
  svgElement: ElementRef;

  constructor() {
  }

  ngAfterViewInit(): void {
    this.setPosition(this.value);
  }

  ngOnChanges(): void {
    if (this.svgProgressBar != null) {
      this.setPosition(this.value);
    }
  }

  onMouseClick(mouseEvent: MouseEvent): void {
    const width = this.svgElement.nativeElement.getBoundingClientRect().width;
    const componentOffsetX = mouseEvent['clientX'];
    const mouseX = componentOffsetX - this.svgElement.nativeElement.getBoundingClientRect().x;
    this.valueChange.emit(mouseX / width);
  }

  private setPosition(position: number): void {
    this.svgProgressBar.nativeElement.setAttribute('x2', position * 100);
  }
}

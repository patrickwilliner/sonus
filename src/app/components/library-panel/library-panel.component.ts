import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { Library } from 'src/app/model/library.model';
import { Playlist } from 'src/app/model/playlist.model';

const KEY_ENTER = 13;

@Component({
  selector: 'app-library-panel',
  templateUrl: './library-panel.component.html',
  styleUrls: ['./library-panel.component.scss']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LibraryPanelComponent {
  @Input()
  library: Library;

  @Output()
  storeActivePlaylist = new EventEmitter<void>();

  @Output()
  updateStoredPlaylist = new EventEmitter<Playlist>();

  @Output()
  play = new EventEmitter<Playlist>();

  @Output()
  delete = new EventEmitter<Playlist[]>();

  @ViewChildren('name_field')
  nameFieldElements: QueryList<ElementRef>;

  deleteMode = false;
  editMode = false;

  private marked = {
  };

  onClickSave(): void {
    this.storeActivePlaylist.emit();
  }

  onEditPlaylist(playlist: Playlist, event): void {
    this.updateStoredPlaylist.emit({ ...playlist, name: event.target.value });
  }

  onClickPlaylist(playlist: Playlist, event: MouseEvent): void {
    if (this.editMode) {
      const nativeElement = event.target as HTMLInputElement;
      nativeElement.readOnly = false;
      nativeElement.focus();
      nativeElement.select();
    } else if (this.deleteMode) {
      this.marked[playlist.id] = !this.marked[playlist.id];
    } else {
      this.play.emit(playlist);
    }
  }

  isMarked(playlist: Playlist): boolean {
    return this.marked[playlist.id];
  }

  onClickEdit(): void {
    this.deleteMode = false;
    this.editMode = !this.editMode;
  }

  onToggleDeleteMode(): void {
    this.deleteMode = !this.deleteMode;
    this.editMode = false;
  }

  onClickDelete(): void {
    const playlists = [];
    this.library.playlists.forEach(playlist => {
      if (this.marked[playlist.id]) {
        playlists.push(playlist);
      }
    });
    this.delete.emit(playlists);
  }

  onBlurNameField(event: FocusEvent, playlist: Playlist): void {
    const targetElement = event.target as HTMLInputElement;
    this.updateStoredPlaylist.emit({ ...playlist, name: targetElement.value });
    targetElement.readOnly = true;
  }

  onKeyUp(event: KeyboardEvent): void {
    if (event.keyCode === KEY_ENTER) {
      (event.target as HTMLInputElement).blur();
    }
  }
}
import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';

@Component({
  selector: 'app-action-tab',
  templateUrl: './action-tab.component.html',
  styleUrls: ['./action-tab.component.scss']
})
export class ActionTabComponent {
  @Input()
  @HostBinding('class.panel-open')
  panelOpen = false;

  @Output()
  close = new EventEmitter<void>();
}
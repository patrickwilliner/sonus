import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PlayerStatus } from 'src/app/model/player-status.model';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent {
  @Input()
  playerStatus: PlayerStatus;

  @Output()
  clickPrevious = new EventEmitter<void>();

  @Output()
  clickPlay = new EventEmitter<void>();

  @Output()
  clickPause = new EventEmitter<void>();

  @Output()
  clickStop = new EventEmitter<void>();

  @Output()
  clickNext = new EventEmitter<void>();

  shouldShowPlay(): boolean {
    return this.playerStatus !== PlayerStatus.PLAYING;
  }

  shouldShowPause(): boolean {
    return this.playerStatus === PlayerStatus.PLAYING;
  }
}
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, OnChanges, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent implements OnChanges {
  @Input()
  label: string;

  @Input()
  value: boolean;

  @Output()
  valueChange = new EventEmitter<boolean>();

  @HostBinding('class.checked')
  checked = true;

  ngOnChanges(): void {
    this.checked = this.value;
  }

  onClick(): void {
    this.valueChange.emit(!this.value);
  }
}
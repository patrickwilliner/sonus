import { AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { PlayerStatus } from 'src/app/model/player-status.model';
import { AudioService } from 'src/app/services/audio.service';

const FFT_SIZE = 64;
const VISUALISATION_COLOR = '#f93';

@Component({
  selector: 'app-spectrum-animator',
  templateUrl: './spectrum-animator.component.html',
  styleUrls: ['./spectrum-animator.component.scss']
})
export class SpectrumAnimatorComponent implements AfterViewInit, OnDestroy {
  @Input()
  playerStatus$: Observable<PlayerStatus>;

  @ViewChild('canvas')
  canvasElement: ElementRef;

  private subscriptions: Subscription[] = [];
  private visualisationRunning = false;
  private audioSource: MediaElementAudioSourceNode;
  private connectedDevices = [];

  constructor(private audioService: AudioService) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
    this.cleanAudioContext();
  }

  ngAfterViewInit(): void {
    if (this.playerStatus$ != null) {
      this.subscriptions.push(this.playerStatus$.subscribe(playerStatus => {
        if (playerStatus === PlayerStatus.PLAYING) {
          if (!this.visualisationRunning) {
            this.startVisualisation();
          }
        } else {
          this.stopVisualisation();
        }
      }));
    }
  }

  private doDraw(canvasCtx, canvas, dataArray) {
    const gap = 2;
    const barWidth = 5;

    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
    for (let i = 0; i < dataArray.length; i++) {
      canvasCtx.fillStyle = VISUALISATION_COLOR;
      canvasCtx.fillRect(i * barWidth + i * gap, 0, barWidth, canvas.height * dataArray[i] / 256);
    }
  }

  private clearCanvas(): void {
    const canvas = this.canvasElement.nativeElement;
    const canvasCtx = canvas.getContext("2d");
    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);
  }

  private startVisualisation() {
    this.audioSource = this.audioService.getAudioSource();

    if (this.audioSource != null) {
      const analyser = this.audioSource.context.createAnalyser();
      this.audioSource.connect(analyser);
      this.audioSource.connect(this.audioSource.context.destination);

      this.connectedDevices.push(analyser);
      this.connectedDevices.push(this.audioSource.context.destination);

      analyser.fftSize = FFT_SIZE;
      const bufferLength = analyser.frequencyBinCount;
      const dataArray = new Uint8Array(bufferLength);

      this.visualisationRunning = true;
      const canvas = this.canvasElement.nativeElement;
      const canvasCtx = canvas.getContext("2d")
      this.draw(dataArray, analyser, canvasCtx, canvas);
    }
  }

  private stopVisualisation() {
    if (this.visualisationRunning) {
      this.visualisationRunning = false;
      this.cleanAudioContext();
      this.clearCanvas();
    }
  }

  private cleanAudioContext(): void {
    if (this.audioSource != null) {
      this.connectedDevices.forEach(device => {
        this.audioSource.disconnect(device);
      });
      this.connectedDevices = [];
    }
  }

  private draw(dataArray, analyser, canvasCtx, canvas) {
    if (this.visualisationRunning) {
      requestAnimationFrame(() => {
        this.draw(dataArray, analyser, canvasCtx, canvas);
      });

      analyser.getByteFrequencyData(dataArray);
      this.doDraw(canvasCtx, canvas, dataArray);
    }
  }
}
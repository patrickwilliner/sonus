import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpectrumAnimatorComponent } from './spectrum-animator.component';
import { AudioService } from 'src/app/services/audio.service';

describe('SpectrumAnimatorComponent', () => {
  let component: SpectrumAnimatorComponent;
  let fixture: ComponentFixture<SpectrumAnimatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpectrumAnimatorComponent ],
      providers: [{ provide: AudioService, useValue: {} }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpectrumAnimatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

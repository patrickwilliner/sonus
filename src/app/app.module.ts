import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { ActionPanelComponent } from './components/action-panel/action-panel.component';
import { ActionTabComponent } from './components/action-tab/action-tab.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { ConfigButtonComponent } from './components/config-button/config-button.component';
import { ConfigPanelComponent } from './components/config-panel/config-panel.component';
import { LibraryButtonComponent } from './components/library-button/library-button.component';
import { LibraryPanelComponent } from './components/library-panel/library-panel.component';
import { LoadButtonComponent } from './components/load-button/load-button.component';
import { NotificationComponent } from './components/notification/notification.component';
import { PlayerComponent } from './components/player/player.component';
import { PlayingNowComponent } from './components/playing-now/playing-now.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';
import { RadioPanelComponent } from './components/radio-panel/radio-panel.component';
import { RepeatButtonComponent } from './components/repeat-button/repeat-button.component';
import { ShuffleButtonComponent } from './components/shuffle-button/shuffle-button.component';
import { SliderComponent } from './components/slider/slider.component';
import { SongMetaPictureComponent } from './components/song-meta-picture/song-meta-picture.component';
import { SpectrumAnimatorComponent } from './components/spectrum-animator/spectrum-animator.component';
import { StatusComponent } from './components/status/status.component';
import { TimerComponent } from './components/timer/timer.component';
import { DurationPipe } from './pipes/duration.pipe';
import { PluralizePipe } from './pipes/pluralize.pipe';
import { AudioService } from './services/audio.service';
import { BusinessDelegateService } from './services/business-delegate.service';
import { ConfigService } from './services/config.service';
import { DropFilesService } from './services/drop-files.service';
import { ErrorHandlerService } from './services/error-handler.service';
import { InitService } from './services/init.service';
import { KeyHandlerService } from './services/key-handler.service';
import { LibraryService } from './services/library.service';
import { LoaderService } from './services/loader.service';
import { MediaSessionService } from './services/media-session.service';
import { NotificationService } from './services/notification.service';
import { PlayerService } from './services/player.service';
import { PlaylistService } from './services/playlist.service';
import { RadioService } from './services/radio.service';
import { StorageService } from './services/storage.service';
import { TimerService } from './services/timer.service';
import { ProgressComponent } from './components/progress/progress.component';
import { ProgressService } from './services/progress.service';
import { VolumeComponent } from './components/volume/volume.component';

const importModules = [
  BrowserModule,
  BrowserAnimationsModule
];

const components = [
  AppComponent,
  PlaylistComponent,
  LoadButtonComponent,
  PlayerComponent,
  TimerComponent,
  SliderComponent,
  StatusComponent,
  PlayingNowComponent,
  ConfigButtonComponent,
  ConfigPanelComponent,
  SpectrumAnimatorComponent,
  CheckboxComponent,
  SongMetaPictureComponent,
  RadioButtonComponent,
  RadioPanelComponent,
  ActionPanelComponent,
  RepeatButtonComponent,
  ShuffleButtonComponent,
  LibraryButtonComponent,
  LibraryPanelComponent,
  ActionTabComponent,
  NotificationComponent,
  ProgressComponent,
  VolumeComponent
];

const pipes = [
  PluralizePipe,
  DurationPipe
];

const services = [
  LoaderService,
  PlayerService,
  ConfigService,
  PlaylistService,
  KeyHandlerService,
  RadioService,
  AudioService,
  TimerService,
  StorageService,
  InitService,
  LibraryService,
  MediaSessionService,
  BusinessDelegateService,
  NotificationService,
  ErrorHandlerService,
  DropFilesService,
  ProgressService
];

@NgModule({
  declarations: [
    ...components,
    ...pipes
  ],
  imports: [
    ...importModules,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [...services],
  bootstrap: [AppComponent]
})
export class AppModule {
}
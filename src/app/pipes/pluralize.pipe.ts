import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'pluralize'})
export class PluralizePipe implements PipeTransform {
  transform(text: string, value: number): string {
    if (value != null) {
      return value === 1 ? text : text + 's';
    } else {
      return '';
    }
  }
}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'duration' })
export class DurationPipe implements PipeTransform {
  transform(seconds: number): string {
    return seconds != null ? this.format(seconds) : '';
  }

  private format(seconds: number): string {
    const secondsRemainder = seconds % 60;
    const minutes = (seconds - secondsRemainder) / 60;
    return `${this.pad(minutes, 2)} : ${this.pad(secondsRemainder, 2)}`;
  }

  private pad(value: number, width: number): string {
    const z = '0';
    const valueString = value + '';

    if (valueString.length >= width) {
      return valueString;
    } else {
      return new Array(width - valueString.length + 1).join(z) + valueString;
    }
  }
}
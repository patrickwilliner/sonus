import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Config } from '../model/config.model';

const LOCAL_STORAGE_KEY = 'config';
const INITIAL_CONFIG = {
  autoplayOnFileLoad: false,
  timerModeIsElapsedTime: false,
  restorePlaylistOnStartup: true,
  audioVolume: 1
} as Config;

@Injectable()
export class ConfigService {
  private config = INITIAL_CONFIG;
  private configSubject = new BehaviorSubject<Config>(this.config);

  getConfig(): Config {
    return this.config;
  }

  getConfig$(): Observable<Config> {
    return this.configSubject.asObservable();
  }

  updateConfig(config: Config): void {
    this.setConfig({
      ...this.config,
      ...config
    });
  }

  load(): void {
    const config = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    this.setConfig({...this.config, ...config}, false);
  }

  private setConfig(config: Config, save = true): void {
    this.config = config;
    this.configSubject.next(config);

    if (save) {
      this.save(config);
    }
  }

  private save(config: Config): void {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(config));
  }
}
import { Injectable } from '@angular/core';
import { BusinessDelegateService } from './business-delegate.service';
import { ConfigService } from './config.service';
import { DropFilesService } from './drop-files.service';
import { KeyHandlerService } from './key-handler.service';
import { LibraryService } from './library.service';
import { MediaSessionService } from './media-session.service';
import { RadioService } from './radio.service';

@Injectable()
export class InitService {
  constructor(private configService: ConfigService,
              private keyHandlerService: KeyHandlerService,
              private businessDelegateService: BusinessDelegateService,
              private libraryService: LibraryService,
              private mediaSessionService: MediaSessionService,
              private dropFilesService: DropFilesService,
              private radioService: RadioService) {
  }

  init(): void {
    this.enableConfigService();
    this.enableLibraryService();
    this.enableSessionPlaylist();
    // this.enableRadioService();
    this.enableDropFiles();
    this.enableMediaSessionService();
    // this.enableKeyHandler();
  }

  private enableConfigService(): void {
    if ('localStorage' in window) {
      this.configService.load();
    }
  }

  private enableLibraryService(): void {
    if ('indexedDB' in window) {
      this.libraryService.enable();
    }
  }

  private enableSessionPlaylist(): void {
    if ('indexedDB' in window) {
      this.businessDelegateService.loadSessionPlaylistOnStartup();
    }
  }

  private enableRadioService(): void {
    this.radioService.enable();
  }

  private enableDropFiles(): void {
    this.dropFilesService.register();
  }

  private enableKeyHandler(): void {
    this.keyHandlerService.enable();
  }

  private enableMediaSessionService(): void {
    if ('mediaSession' in navigator) {
      this.mediaSessionService.enable();
    }
  }
}
import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { BusinessDelegateService } from './business-delegate.service';

@Injectable()
export class DropFilesService {
  constructor(@Inject(DOCUMENT) private document: Document, private businessDelegateService: BusinessDelegateService) {
  }

  register(): void {
    window.addEventListener('dragover', this.onDragOver.bind(this), false);
    window.addEventListener('drop', this.onDrop.bind(this), false);
  }

  private onDragOver(event: any): void {
    event.preventDefault();
  }

  private onDrop(event: any): void {
    const files = this.extractFiles(event.dataTransfer);
    this.businessDelegateService.loadFilesToSessionPlaylist(files);
    event.preventDefault();
  }

  private extractFiles(dataTransfer: DataTransfer): File[] {
    const files: File[] = [];

    if (dataTransfer.items?.length != null) {
      for (let i = 0; i < dataTransfer.items.length; i++) {
        files.push(dataTransfer.items[i].getAsFile());
      }
    }

    return files;
  }
}
import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { PlayerStatus } from '../model/player-status.model';
import { AudioService } from './audio.service';
import { PlaylistService } from './playlist.service';
import { StorageService } from './storage.service';

@Injectable()
export class PlayerService {
  private playerStatus: PlayerStatus = PlayerStatus.STOPPED;
  private playerStatusSubject = new BehaviorSubject(this.playerStatus);

  constructor(private audioService: AudioService,
              private playlistService: PlaylistService,
              private storageService: StorageService) {
  }

  getPlayerStatus$(): Observable<PlayerStatus> {
    return this.playerStatusSubject.asObservable();
  }

  play(): void {
    if (this.playerStatus === PlayerStatus.PAUSED) {
      this.audioService.getAudio().play();
      this.setPlayerStatus(PlayerStatus.PLAYING);
    } else {
      const song = this.playlistService.getCurrentSong();
      this.storageService.getSongUrl(song).pipe(
        first(),
        map(playableUrl => this.audioService.createNewAudio(playableUrl))
      ).subscribe(audio => {
        this.createAudioHasEndedListener(audio);
        audio.play();
        this.setPlayerStatus(PlayerStatus.PLAYING);
      });
    }
  }

  playSong(index: number): void {
    this.stop();
    this.playlistService.selectSong(index);
    this.play();
  }

  playNext(): void {
    const nextSongIndex = this.playlistService.getNextSongIndex();
    if (nextSongIndex != null) {
      this.playSong(nextSongIndex);
    }
  }

  playPrevious(): void {
    const previousSongIndex = this.playlistService.getPreviousSongIndex();
    if (previousSongIndex != null) {
      this.playSong(previousSongIndex);
    }
  }

  pause(): void {
    this.setPlayerStatus(PlayerStatus.PAUSED);
    this.audioService.getAudio()?.pause();
  }

  stop(): void {
    this.setPlayerStatus(PlayerStatus.STOPPED);
    this.audioService.destroyAudio();
  }

  navigateToTime(time: number): void {
    if (this.playerStatus !== PlayerStatus.STOPPED) {
      this.audioService.getAudio().currentTime = time;
    }
  }

  private setPlayerStatus(playerStatus: PlayerStatus): void {
    this.playerStatus = playerStatus;
    this.playerStatusSubject.next(playerStatus);
  }

  private createAudioHasEndedListener(audio: HTMLAudioElement): void {
    fromEvent(audio, 'ended').pipe(first()).subscribe(this.handleAudioHasEnded.bind(this));
  }

  private handleAudioHasEnded(): void {
    this.stop();
    this.playNext();
  }
}
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class ProgressService {
  private progressSubject = new BehaviorSubject(false);

  isEnabled$(): Observable<boolean> {
    return this.progressSubject.asObservable();
  }

  enable(): void {
    this.progressSubject.next(true);
  }

  disable(): void {
    this.progressSubject.next(false);
  }
}
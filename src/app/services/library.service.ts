import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { Library } from '../model/library.model';
import { Playlist } from '../model/playlist.model';
import { PlaylistService } from './playlist.service';
import { StorageService } from './storage.service';

const EMPTY_LIBRARY = {
  playlists: []
};

@Injectable()
export class LibraryService {
  private serviceEnabled = false;
  private library: Library = EMPTY_LIBRARY;
  private librarySubject = new BehaviorSubject<Library>(this.library);

  constructor(private storageService: StorageService, private playlistService: PlaylistService) {
  }

  enable(): void {
    if (!this.serviceEnabled) {
      this.loadLibrary();
      this.serviceEnabled = true;
    }
  }

  isEnabled(): boolean {
    return this.serviceEnabled;
  }

  getLibrary$(): Observable<Library> {
    return this.librarySubject.asObservable();
  }

  saveSessionPlaylist(): void {
    const playlist = this.playlistService.getPlaylist();
    this.storageService.storePlaylistToLibrary(playlist).pipe(first()).subscribe(storedPlaylist => {
      this.library.playlists.push(storedPlaylist);
      this.librarySubject.next(this.library);
    });
  }

  setPlaylists(playlists: Playlist[]): void {
    this.library.playlists = playlists;
    this.librarySubject.next(this.library);
  }

  updatePlaylist(playlist: Playlist): void {
    this.library.playlists = this.getUpdated(this.library.playlists, playlist);
    this.librarySubject.next(this.library);
    this.storageService.updatePlaylist(playlist);
  }

  deletePlaylists(playlists: Playlist[]): void {
    this.storageService.deletePlaylistsFromLibrary(playlists).pipe(first()).subscribe(() => {
      this.loadLibrary();
    });
  }

  loadPlaylistToSession(playlist: Playlist): void {
    this.storageService.loadLibraryPlaylistFiles(playlist.id).pipe(first()).subscribe();
  }

  private getUpdated(playlists: Playlist[], playlist: Playlist): Playlist[] {
    const result = [];
    playlists.forEach(pl => {
      if (pl.id === playlist.id) {
        result.push({ ...pl, name: playlist.name });
      } else {
        result.push(pl);
      }
    });
    return result;
  }

  private loadLibrary(): void {
    this.storageService.loadLibraryPlaylists().pipe(first()).subscribe(playlists => {
      this.setPlaylists(playlists);
    });
  }
}
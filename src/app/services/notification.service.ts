import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Notification } from '../model/notification.model';

@Injectable()
export class NotificationService {
  private notificationSubject = new Subject<Notification>();

  notify(notification: Notification): void {
    this.notificationSubject.next(notification);
  }

  getNotifications$(): Observable<Notification> {
    return this.notificationSubject.asObservable();
  }
}
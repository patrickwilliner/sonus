import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, OnDestroy } from "@angular/core";
import { fromEvent, Subscription } from 'rxjs';
import { PlayerService } from './player.service';

const KEY_CODE_SPACE = 32;

@Injectable()
export class KeyHandlerService implements OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(private playerService: PlayerService, @Inject(DOCUMENT) private document: Document) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  enable(): void {
    this.registerCustomKeyHandler();
  }

  private registerCustomKeyHandler(): void {
    this.subscriptions.push(fromEvent(this.document, 'keypress').subscribe(this.handleKeyboardEvent.bind(this)));
  }

  private handleKeyboardEvent(event: KeyboardEvent): void {
  }
}
import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { Playlist } from '../model/playlist.model';
import { ConfigService } from './config.service';
import { LibraryService } from './library.service';
import { LoaderService } from './loader.service';
import { PlayerService } from './player.service';
import { PlaylistService } from './playlist.service';
import { ProgressService } from './progress.service';
import { StorageService } from './storage.service';

@Injectable()
export class BusinessDelegateService {
  constructor(private loaderService: LoaderService,
              private playerService: PlayerService,
              private configService: ConfigService,
              private storageService: StorageService,
              private playlistService: PlaylistService,
              private progressService: ProgressService,
              private libraryService: LibraryService) {
  }

  loadFilesToSessionPlaylist(fileList: FileList | File[]): void {
    this.playlistService.clear();
    this.progressService.enable();
    this.playerService.stop();

    let files: File[];
    if (fileList instanceof FileList) {
      files = this.mapFileListToFileArray(fileList);
    } else {
      files = fileList;
    }

    const observable = this.loaderService.loadFilesToSessionPlaylist(files);
    observable.pipe(first()).subscribe(() => {
      this.progressService.disable();

      if (this.configService.getConfig().autoplayOnFileLoad) {
        this.playerService.play();
      }
    });
  }

  loadSessionPlaylistOnStartup(): void {
    if (this.configService.getConfig().restorePlaylistOnStartup) {
      this.storageService.loadSession().pipe(first()).subscribe(playlist => {
        this.playlistService.setPlaylist(playlist);
      });
    }
  }

  playLibraryPlaylist(playlist: Playlist): void {
    this.storageService.clearSession().pipe(first()).subscribe(() => {
      this.libraryService.loadPlaylistToSession(playlist);
      this.storageService.saveSession(playlist).subscribe(savedPlaylist => {
        this.playlistService.setPlaylist(savedPlaylist);
        this.playerService.play();
      });
    });
  }

  private mapFileListToFileArray(fileList: FileList): File[] {
    const files = [];
    for (let i = 0; i < fileList.length; i++) {
      files.push(fileList.item(i));
    }
    return files;
  }
}
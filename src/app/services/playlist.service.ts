import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { Playlist } from '../model/playlist.model';
import { RepeatMode } from '../model/repeat-status.model';
import { Song } from '../model/song.model';

const EMPTY_PLAYLIST = {
  id: undefined,
  name: '',
  songs: [],
  selectedSong: 0,
  shuffle: false,
  repeat: RepeatMode.NONE
}

@Injectable()
export class PlaylistService {
  private playlist: Playlist = EMPTY_PLAYLIST;
  private playlistSubject: Subject<Playlist> = new BehaviorSubject(this.playlist);

  clear(): void {
    this.setPlaylist(EMPTY_PLAYLIST);
  }

  getCurrentSong(): Song {
    if (this.playlist?.songs?.length > 0) {
      return this.playlist.songs[this.playlist.selectedSong];
    } else {
      return undefined;
    }
  }

  getPlaylist$(): Observable<Playlist> {
    return this.playlistSubject.asObservable();
  }

  getPlaylist(): Playlist {
    return this.playlist;
  }

  setPlaylist(playlist: Playlist): void {
    this.playlist = playlist;
    this.playlistSubject.next(playlist);
  }

  selectSong(index: number): void {
    this.playlist.selectedSong = index;
    this.playlistSubject.next(this.playlist);
  }

  toggleShuffle(): void {
    this.playlist.shuffle = !this.playlist.shuffle;
    this.playlistSubject.next(this.playlist);
  }

  toggleRepeatMode(): void {
    this.playlist.repeat = this.getToggledRepeatMode(this.playlist.repeat);
    this.playlistSubject.next(this.playlist);
  }

  createNewPlaylist(songs: Song[]): Playlist {
    return {
      id: undefined,
      name: 'New Playlist',
      songs: songs,
      selectedSong: 0,
      shuffle: false,
      repeat: RepeatMode.NONE
    };
  }

  getNextSongIndex(): number {
    if (this.playlist.repeat === RepeatMode.CURRENT_SONG) {
      return this.playlist.selectedSong;
    } else if (this.playlist.shuffle) {
      return this.getRandomIndex(this.playlist);
    } else if (!this.selectedSongIsLast(this.playlist)) {
      return this.playlist.selectedSong + 1;
    } else if (this.playlist.repeat === RepeatMode.PLAYLIST) {
      return 0;
    } else {
      return undefined;
    }
  }

  getPreviousSongIndex(): number {
    if (this.playlist.selectedSong > 0) {
      return this.playlist.selectedSong - 1;
    } else {
      return undefined;
    }
  }

  private getRandomIndex(playlist: Playlist): number {
    return Math.floor(Math.random() * Math.floor(playlist.songs.length));
  }

  private selectedSongIsLast(playlist: Playlist): boolean {
    return playlist.selectedSong === playlist.songs.length - 1;
  }

  private getToggledRepeatMode(repeatMode: RepeatMode): RepeatMode {
    if (repeatMode === RepeatMode.NONE) {
      return RepeatMode.PLAYLIST;
    } else if (repeatMode === RepeatMode.PLAYLIST) {
      return RepeatMode.CURRENT_SONG;
    } else {
      return RepeatMode.NONE;
    }
  }
}
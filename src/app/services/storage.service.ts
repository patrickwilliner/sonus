import { Injectable } from '@angular/core';
import { combineLatest, from, Observable, Subject } from 'rxjs';
import { first, flatMap, map, mergeMap, tap, toArray } from 'rxjs/operators';

import { Playlist } from '../model/playlist.model';
import { Song } from '../model/song.model';

const DB_NAME = 'music_data';
const DB_VERSION = 1;
const OBJECTSTORE_SESSION = 'session'
const OBJECTSTORE_SESSION_FILES = 'session_files'
const OBJECTSTORE_LIBRARY = 'library'

@Injectable()
export class StorageService {
  getSongUrl(song: Song): Observable<string> {
    return this.loadFile(song).pipe(map(fileAndId => {
      return window.URL.createObjectURL(fileAndId.file);
    }));
  }

  storeFile(file: File, temporary: boolean): Observable<string> {
    return this.getDbRequest().pipe(
      flatMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION_FILES, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION_FILES);
        return this.asObservable(os.add(file));
      }),
      map(event => event.target['result'])
    );
  }

  saveSession(playlist: Playlist): Observable<Playlist> {
    return this.getDbRequest().pipe(
      flatMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION);
        return this.asObservable(os.add(playlist));
      }),
      map(event => {
        return {
          ...playlist,
          id: event.target['result']
        };
      })
    );
  }

  loadSession(): Observable<Playlist> {
    return this.getDbRequest().pipe(
      flatMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION);
        return this.asObservable(os.getAll());
      }),
      map(event => event.target['result'][0])
    );
  }

  clearSession(): Observable<boolean> {
    return this.getDbRequest().pipe(
      flatMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION);
        return this.asObservable(os.clear());
      }),
      map(event => {
        const db: IDBDatabase = event.target['transaction'].db;
        const transaction = db.transaction(OBJECTSTORE_SESSION_FILES, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION_FILES);
        return this.asObservable(os.clear());
      }),
      map(() => true)
    );
  }

  // saveSongs(songs: Song[]): void {
  //   let request = this.getDbRequest();
  //   request.onsuccess = event => {
  //     let db: IDBDatabase = event.target['result'];
  //     let transaction = db.transaction(OBJECTSTORE_PLAYLIST, 'readwrite');
  //     let os = transaction.objectStore(OBJECTSTORE_PLAYLIST);
  //     os.clear();
  //     os.add(songs);
  //     db.close();
  //   };
  // }

  storePlaylistToLibrary(playlist: Playlist): Observable<Playlist> {
    return combineLatest([this.getDbRequest(), this.getPlaylistSongFiles(playlist)]).pipe(
      mergeMap(pair => {
        const db: IDBDatabase = pair[0].target['result'];
        const transaction = db.transaction(OBJECTSTORE_LIBRARY, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_LIBRARY);

        const playlistToStore = playlist;
        delete playlistToStore.id;

        return this.asObservable(os.add({
          playlist: playlistToStore,
          files: pair[1]
        }));
      }),
      map(event => {
        return {
          ...playlist,
          id: event.target['result']
        };
      })
    );
  }

  loadLibraryPlaylists(): Observable<Playlist[]> {
    let subject = new Subject<Playlist[]>();

    this.getDbRequest().subscribe(event => {
      const db: IDBDatabase = event.target['result'];
      const transaction = db.transaction(OBJECTSTORE_LIBRARY, 'readwrite');
      const os = transaction.objectStore(OBJECTSTORE_LIBRARY);
      const playlists = [];
      const request = os.openCursor();
      request.onsuccess = event => {
        const cursor = event.target['result'];
        if (cursor) {
          const playlist = cursor.value.playlist;
          playlist.id = cursor.key;
          playlists.push(playlist);
          cursor.continue();
        } else {
          subject.next(playlists);
          subject.complete();
        }
      };
      request.onerror = error => {
        console.log(error);
      };
    });

    return subject.asObservable();
  }

  loadLibraryPlaylistFiles(playlistId: string): Observable<boolean> {
    return this.getDbRequest().pipe(
      flatMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_LIBRARY, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_LIBRARY);
        return this.asObservable(os.get(playlistId));
      }),
      map(event => {
        const db: IDBDatabase = event.target['transaction'].db;
        const transaction = db.transaction(OBJECTSTORE_SESSION_FILES, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION_FILES);
        const filesAndIds = event.target['result'].files;

        filesAndIds.forEach(fileAndId => {
          os.put(fileAndId.file, fileAndId.id);
        });

        return true;
      })
    );
  }

  updatePlaylist(playlist: Playlist): void {
    this.getDbRequest().pipe(first()).subscribe(dbRequestEvent => {
      const db: IDBDatabase = (dbRequestEvent.target as IDBOpenDBRequest).result;
      const transaction = db.transaction(OBJECTSTORE_LIBRARY, 'readwrite');
      const os = transaction.objectStore(OBJECTSTORE_LIBRARY);

      os.get(playlist.id).onsuccess = playlistLoadEvent => {
        const data = (playlistLoadEvent.target as IDBRequest).result;
        data.playlist.name = playlist.name;
        os.put(data, playlist.id);
      };
    });
  }

  deletePlaylistsFromLibrary(playlists: Playlist[]): Observable<void> {
    return this.getDbRequest().pipe(
      map(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_LIBRARY, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_LIBRARY);

        playlists.forEach(playlist => {
          const pdestroy = os.openKeyCursor(IDBKeyRange.only(playlist.id));
          pdestroy.onsuccess = () => {
            const cursor = pdestroy.result;
            if (cursor) {
              const request = os.delete(cursor.primaryKey);
              request.onerror = () => {
                console.log(request.error);
              };
              request.onsuccess = () => {
                console.log('delete ok', playlist.name);
              };
              cursor.continue();
            }
          };
        });
      })
    );
  }

  private getPlaylistSongFiles(playlist: Playlist): Observable<FileAndId[]> {
    return from(playlist.songs).pipe(
      mergeMap(song => this.loadFile(song)),
      toArray()
    );
  }

  private loadFile(song: Song): Observable<FileAndId> {
    return this.getDbRequest().pipe(
      mergeMap(event => {
        const db: IDBDatabase = event.target['result'];
        const transaction = db.transaction(OBJECTSTORE_SESSION_FILES, 'readwrite');
        const os = transaction.objectStore(OBJECTSTORE_SESSION_FILES);
        return this.asObservable(os.get(song.fileId));
      }),
      map(event => {
        return {
          id: song.fileId,
          file: event.target['result']
        };
      })
    );
  }

  private getDbRequest(): Observable<Event> {
    const subject = new Subject<Event>();
    const request = window.indexedDB.open(DB_NAME, DB_VERSION);
    request.onupgradeneeded = this.createDatabaseSchema.bind(this);
    request.onsuccess = event => {
      subject.next(event);
      subject.complete();
    };
    request.onerror = error => {
      subject.error(error);
    };
    return subject.asObservable();
  }

  private createDatabaseSchema(event: any): void {
    const db: IDBDatabase = event.target['result'];
    db.createObjectStore(OBJECTSTORE_SESSION, { autoIncrement: true });
    db.createObjectStore(OBJECTSTORE_LIBRARY, { autoIncrement: true });
    db.createObjectStore(OBJECTSTORE_SESSION_FILES, { autoIncrement: true });
  }

  private asObservable(request: IDBRequest<any>): Observable<Event> {
    const subject = new Subject<any>();

    request.onsuccess = event => {
      subject.next(event);
      subject.complete();
    };

    request.onerror = error => {
      subject.error(error);
    };

    return subject.asObservable();
  }
}

interface FileAndId {
  id: string;
  file: File;
}
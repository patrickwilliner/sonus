import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ImageUtil } from '../lib/util/image-util';
import { Song } from '../model/song.model';
import { PlayerService } from './player.service';
import { PlaylistService } from './playlist.service';

const ACTION_MEDIA_PLAY = 'play';
const ACTION_MEDIA_PAUSE = 'pause';
const ACTION_MEDIA_PREVIOUS = 'previoustrack';
const ACTION_MEDIA_NEXT = 'nexttrack';

@Injectable()
export class MediaSessionService implements OnDestroy {
  private subscriptions: Subscription[] = [];
  private lastImageBlobUrl: string;

  constructor(private playlistService: PlaylistService, private playerService: PlayerService) {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  enable() {
    this.registerPlaylistHandler();
    this.registerMediaKeyHandler();
  }

  private registerPlaylistHandler(): void {
    this.subscriptions.push(this.playlistService.getPlaylist$().subscribe(playlist => {
      if ((playlist?.songs ?? []).length > 0) {
        const currentSong = playlist.songs[playlist.selectedSong];
        navigator['mediaSession'].metadata = new MediaMetadata(this.buildMetaData(currentSong));
      }
    }));
  }

  private registerMediaKeyHandler(): void {
    const mediaSession = navigator['mediaSession'];

    if (mediaSession != null) {
      mediaSession.setActionHandler(ACTION_MEDIA_PLAY, this.onMediaPlay.bind(this));
      mediaSession.setActionHandler(ACTION_MEDIA_PAUSE, this.onMediaPause.bind(this));
      mediaSession.setActionHandler(ACTION_MEDIA_PREVIOUS, this.onMediaPrevious.bind(this));
      mediaSession.setActionHandler(ACTION_MEDIA_NEXT, this.onMediaNext.bind(this));
    }
  }

  private onMediaPlay() {
    this.playerService.play();
  }

  private onMediaPause() {
    this.playerService.pause();
  }

  private onMediaPrevious() {
    this.playerService.playPrevious();
  }

  private onMediaNext() {
    this.playerService.playNext();
  }

  private buildMetaData(song: Song): any {
    const metaData = {
      title: '',
      artist: '',
      album: '',
      artwork: []
    };

    if (song.songMeta?.picture != null) {
      URL.revokeObjectURL(this.lastImageBlobUrl);

      const bytes = song.songMeta.picture['data'];
      const mimeType = song.songMeta.picture['type'];
      const imageUrl = ImageUtil.buildBlobUrl(bytes, mimeType);
      metaData.artwork = [{ src: imageUrl, sizes: '96x96', type: mimeType }];

      this.lastImageBlobUrl = imageUrl;
    }

    if (song.songMeta?.title) {
      metaData.title = song.songMeta.title;
    } else if (song.fileMeta?.name != null) {
      metaData.title = song.fileMeta.name;
    }

    if (song.songMeta?.artist) {
      metaData.artist = song.songMeta.artist;
    }

    if (song.songMeta?.album) {
      metaData.album = song.songMeta.album;
    }

    return metaData;
  }
}
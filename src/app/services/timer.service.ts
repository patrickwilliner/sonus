import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, interval, Observable, Subject, Subscription, fromEvent, Scheduler } from 'rxjs';
import { map } from 'rxjs/operators';
import { PlayerStatus } from '../model/player-status.model';
import { AudioService } from './audio.service';
import { PlayerService } from './player.service';

const TIMER_INTERVAL_MS = 100;

@Injectable()
export class TimerService implements OnDestroy {
  private timerSubject: Subject<number> = new BehaviorSubject(0);
  private timerSubscription: Subscription;
  private subscriptions: Subscription[] = [];

  constructor(private audioService: AudioService, playerService: PlayerService) {
    this.subscribeToPlayer(playerService.getPlayerStatus$());
  }

  ngOnDestroy(): void {
    this.timerSubscription.unsubscribe();
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getTimer(): Observable<number> {
    return this.timerSubject.asObservable();
  }

  private subscribeToPlayer(player$: Observable<PlayerStatus>): void {
    this.subscriptions.push(player$.subscribe(playerStatus => {
      if (playerStatus === PlayerStatus.PLAYING) {
        this.start();
      } else {
        this.stop();
      }
    }));
  }

  private start(): void {
    if (this.timerSubscription == null) {
      this.timerSubscription = interval(TIMER_INTERVAL_MS)
        .pipe(map(() => this.audioService.getAudio()?.currentTime || 0))
        .subscribe(currentTime => this.timerSubject.next(Math.floor(currentTime)));
    }
  }

  private stop(): void {
    // TODO two issues with this: 1) sometimes at stop the timer is not at 0 and when
    // paused and replayed by the media keys the timer doesnt update anymore
    // this.timerSubscription?.unsubscribe();
    // delete this.timerSubscription;
  }
}
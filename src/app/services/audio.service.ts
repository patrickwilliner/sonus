import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RadioStation } from '../model/radio-station.model';
import { ConfigService } from './config.service';

@Injectable()
export class AudioService implements OnDestroy {
  private audioSource: MediaElementAudioSourceNode;
  private audio: HTMLAudioElement;

  private subscriptions: Subscription[] = [];

  constructor(private configService: ConfigService) {
    this.subscriptions.push(configService.getConfig$().subscribe(config => {
      if (this.audio != null) {
        this.audio.volume = config.audioVolume;
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  getAudio(): HTMLAudioElement {
    return this.audio;
  }

  getAudioSource(): MediaElementAudioSourceNode {
    return this.audioSource;
  }

  createNewAudio(playableUrl: string): HTMLAudioElement {
    this.destroyAudio();
    this.audio = new Audio(playableUrl);
    this.audioSource = new AudioContext().createMediaElementSource(this.audio);
    this.audio.volume = this.configService.getConfig().audioVolume;
    return this.audio;
  }

  createNewAudioFromRadioStation(radioStation: RadioStation): HTMLAudioElement {
    this.destroyAudio();
    const audio = new Audio(radioStation.urls[0]);
    audio.crossOrigin = 'anonymous';
    return audio;
  }

  destroyAudio(): void {
    if (this.audio != null) {
      delete this.audioSource;
      this.audio.pause();
      this.audio.load();
      delete this.audio;
    }
  }
}
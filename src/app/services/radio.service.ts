import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RadioStation } from '../model/radio-station.model';
import { Radio } from '../model/radio.model';


const radioStations = [{
  name: 'RadioSky-Music Jazz',
  urls: [
    'http://listen.radioskymusic.com:7006/live',
    'http://jazz.radioskymusic.com:7006/live',
    'http://live.radioskymusic.com'
  ],
  country: 'Canada',
  language: 'English',
  genre: 'Jazz-Easy Listening'
}, {
  name: '.113FM Bluesville',
  urls: [
    'http://113fm-edge1.cdnstream.com/1816_128'
  ],
  country: 'USA',
  language: 'English',
  genre: 'Blues'
}, {
  name: 'WHRO altRadio',
  urls: [
    'http://altradio.mediaplayer.whro.org/128'
  ],
  country: 'USA',
  language: 'English',
  genre: 'Rock-Alternative'
}];

@Injectable()
export class RadioService {
  private serviceEnabled = false;

  private radio: Radio;
  private radioSubject = new Subject<Radio>();

  enable(): void {
    if (!this.serviceEnabled) {
      this.setRadioStations();
      this.serviceEnabled = true;
    }
  }

  isEnabled(): boolean {
    return this.serviceEnabled;
  }

  getRadio$(): Observable<Radio> {
    return this.radioSubject.asObservable();
  }

  playStream(radioStation: RadioStation): void {
  }

  private setRadioStations(): void {
    this.radio = {
      radioStations: radioStations
    };
    this.radioSubject.next(this.radio);
  }
}
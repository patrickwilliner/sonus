import { Injectable } from '@angular/core';
import { NotificationType } from '../model/notification-type.model';
import { NotificationService } from './notification.service';

@Injectable()
export class ErrorHandlerService {
  constructor(private notificationService: NotificationService) {
  }

  handle(message: string): void {
    this.notificationService.notify({ message: message, type: NotificationType.ERROR });
  }
}
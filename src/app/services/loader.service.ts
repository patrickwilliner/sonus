import { Injectable } from '@angular/core';
import { from, fromEvent, Observable, of } from 'rxjs';
import { first, flatMap, map, toArray } from 'rxjs/operators';

import MetaDataParser from '../lib/meta-data-parsing/meta-data-parser';
import { SongUtil } from '../lib/util/song-util';
import { Song } from '../model/song.model';
import { ErrorHandlerService } from './error-handler.service';
import { PlaylistService } from './playlist.service';
import { StorageService } from './storage.service';

@Injectable()
export class LoaderService {
  constructor(private playlistService: PlaylistService,
              private storageService: StorageService,
              private errorHandlerService: ErrorHandlerService) {
  }

  loadFilesToSessionPlaylist(files: File[]): Observable<void> {
    return this.storageService.clearSession().pipe(
      flatMap(() => this.loadFiles(files)),
      map(songs => this.playlistService.createNewPlaylist(songs)),
      flatMap(playlist => this.storageService.saveSession(playlist)),
      map(playlist => this.playlistService.setPlaylist(playlist))
    );
  }

  private loadFiles(files: File[]): Observable<Song[]> {
    return from(files).pipe(
      map(file => this.createAccumulator(file)),
      flatMap(acc => this.addDuration(acc)),
      flatMap(acc => this.addSongMeta(acc)),
      flatMap(acc => this.storeFileAndAddId(acc)),
      map(acc => this.createSong(acc)),
      toArray(),
      map(songs => songs.sort(SongUtil.compare))
    );
  }

  private storeFileAndAddId(acc: any): Observable<any> {
    return this.storageService.storeFile(acc.file, true).pipe(
      map(id => (acc.fileId = id, acc))
    );
  }

  private createAccumulator(file: File): any {
    return {
      file: file
    };
  }

  private createSong(acc: any): Song {
    return {
      fileMeta: {
        name: acc.file.name,
        size: acc.file.size,
        type: acc.file.type
      },
      songMeta: {
        ...acc.songMeta,
        duration: acc.duration
      },
      fileId: acc.fileId
    };
  }

  private addDuration(acc: any): Observable<any> {
    const url = window.URL.createObjectURL(acc.file);
    const audio = new Audio(url);

    audio.onerror = () => {
      this.errorHandlerService.handle(`Could not load "${acc.file.name}"`);
    };

    return fromEvent(audio, 'loadedmetadata').pipe(
      first(),
      map(() => audio.duration),
      map(Math.floor),
      map(duration => (acc.duration = duration, acc))
    );
  }

  private addSongMeta(acc: any): Observable<any> {
    return new MetaDataParser().parse(acc.file)
      .pipe(map(songMeta => (acc.songMeta = songMeta, acc)));
  }
}
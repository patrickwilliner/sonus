export interface FileMeta {
  name: string;
  type: string;
  size: number;
}
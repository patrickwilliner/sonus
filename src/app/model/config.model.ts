export interface Config {
  autoplayOnFileLoad?: boolean;
  timerModeIsElapsedTime?: boolean;
  restorePlaylistOnStartup?: boolean;
  audioVolume?: number;
}
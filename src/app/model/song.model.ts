import { FileMeta } from './file-meta.model';
import { SongMeta } from './song-meta.model';

export interface Song {
  fileId: string;
  songMeta: SongMeta;
  fileMeta: FileMeta;
}
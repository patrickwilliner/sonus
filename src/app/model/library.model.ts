import { Playlist } from './playlist.model';

export interface Library {
  playlists: Playlist[];
}
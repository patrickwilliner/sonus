import { Playlist } from './playlist.model';

export interface Session {
  playlist: Playlist;
}
export enum RepeatMode {
  NONE,
  CURRENT_SONG,
  PLAYLIST
}
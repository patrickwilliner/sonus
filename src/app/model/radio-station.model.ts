export interface RadioStation {
  name: string;
  urls: string[];
  country?: string;
  language?: string;
  genre?: string;
}
export interface SongMeta {
  title?: string;
  duration?: number;
  track?: string;
  artist?: string;
  album?: string;
  year?: string;
  size?: number;
  format?: string;
  metaTag?: string;
  picture?: number[];
}
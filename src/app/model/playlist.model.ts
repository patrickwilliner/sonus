import { RepeatMode } from './repeat-status.model';
import { Song } from './song.model';

export interface Playlist {
  id: string;
  name: string;
  songs: Song[];
  selectedSong: number;
  shuffle: boolean;
  repeat: RepeatMode;
}
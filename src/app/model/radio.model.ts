import { RadioStation } from './radio-station.model';

export interface Radio {
  radioStations: RadioStation[];
}